#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
import time
import os
import socket
import secrets
import hashlib
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def write_log(line, fichero_log):
    """Funcion para escribir en los ficheros."""
    now = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
    with open(fichero_log, 'a') as log_file:
        log_file.write(now + ' ' + line.replace('\r\n', ' ') + '\n')


def sent_to(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes enviados."""
    line = 'Send to ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def received_from(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes recibidos."""
    line = 'Receive from ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def error(message, fichero_log):
    """Funcion para escribir los errores en los ficheros."""
    line = 'Error: ' + message
    write_log(line, fichero_log)


def starting(fichero_log):
    """Funcion para inicializar procesos."""
    line = 'Starting...'
    write_log(line, fichero_log)


def finishing(fichero_log):
    """Funcion para finalizar procesos."""
    line = 'Finishing.'
    write_log(line, fichero_log)


class XMLReader(ContentHandler):
    """Funcion para parsear ficheros 'xml'."""

    def __init__(self):
        """Funcion para inicializar xml reader."""
        self.current_tag = ''
        self.dicc = {}
        self.attr = {'server': ['name', 'ip', 'puerto'],
                     'database': ['path', 'passwdpath'],
                     'log': ['path']}

    def startElement(self, name, attrs):
        """Funcion que se ejecuta al empezar una etiqueta."""
        self.current_tag = name
        if name in self.attr:

            for atributo in self.attr[name]:
                att = attrs.get(atributo, '')
                if att == '':
                    att = '127.0.0.1'
                self.dicc[name + '_' + atributo] = att

    def endElement(self, name):
        """Funcion que se ejecuta al acabar una etiqueta."""
        self.current_tag = ''

    def characters(self, content):
        """Funcion que se ejecuta para leer el contenido de una etiqueta."""
        if self.current_tag in ['log', 'audio']:
            self.dicc[self.current_tag] = content

    def get_tags(self):
        """Funcion para obtener etiquetas."""
        return self.dicc


class SIPProxyHandler(socketserver.DatagramRequestHandler):
    """Servidor proxy sip."""

    reg = {}
    passwords = {}
    subscriptions = []

    def json2register(self):
        """Funcion para leer del fichero json los clientes activos."""
        try:
            with open(tags['database_path'], 'r') as jsonfile:
                self.reg = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def register2json(self):
        """Funcion para guardar en el fichero json la base de datos."""
        with open(tags['database_path'], 'w') as jsonfile:
            json.dump(self.reg, jsonfile)

    def json2passwords(self):
        """Funcion para leer del fichero json las contraseñas."""
        try:
            with open(tags['database_passwdpath'], 'r') as jsonfile:
                self.passwords = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def notify(self, username):
        """Funcion para enviar mensajes de tipo notify."""
        for client in self.subscriptions:
            mess = 'NOTIFY sip:' + client + ' SIP/2.0\r\nReason: SIP ;'
            mess += 'cause=200 ;text="' + username
            mess += ' just registered"\r\n\r\n'
            self.resent(mess, client)

    def save_client(self, username, expires, ip, port):
        """Funcion para guardar un cliente."""
        if username not in self.reg:
            self.notify(username)
        self.reg[username] = {
            'username': username,
            'ip': ip,
            'port': port,
            'reg_date': time.time() + 3600,
            'expires': expires
        }

    def delete_client(self):
        """Funcion para borrar a los clientes caducados."""
        d = self.reg.copy()
        now = time.time() + 3600
        for client in d:
            expires = self.reg[client]['reg_date']
            expires += self.reg[client]['expires']
            if now >= expires:
                del self.reg[client]

    def register(self, data):
        """Funcion para manejar los mensajes de tipo register."""
        username = data.split()[1].split(':')[1]
        expires = int(data.split()[4])
        port = int(data.split()[1].split(':')[2])
        ip = self.client_address[0]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if username in self.reg:
            if expires == 0:
                del self.reg[username]
                if username in self.subscriptions:
                    self.subscriptions.remove(username)
            else:
                self.save_client(username, expires, ip, port)
            sent_to(client_ip, client_port, 'SIP/2.0 200 OK', fichero_log)
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        else:
            if 'Digest response' in data:
                response = data.split('"')[1]
                if secrets.compare_digest(response,
                                          self.get_response(username)):
                    self.save_client(username, expires, ip, port)
                    line = 'SIP/2.0 200 OK\r\n\r\n'
                    sent_to(client_ip, client_port, line, fichero_log)
                    self.wfile.write(bytes(line, 'utf-8'))

            else:
                mess = 'SIP/2.0 401 Unauthorized\r\nWWW Authenticate: '
                mess += 'Digest nonce="' + self.get_nonce(username)
                mess += '"\r\n\r\n'
                sent_to(client_ip, client_port, mess, fichero_log)
                self.wfile.write(bytes(mess, 'utf-8'))

    def get_nonce(self, username, encoding='utf-8'):
        """Funcion para devolver el digest nonce de un usuario."""
        digest = hashlib.md5()
        digest.update(bytes(username, encoding))
        digest.update(bytes(self.passwords[username], encoding))
        digest.digest()
        return digest.hexdigest()

    def get_response(self, username, encoding='utf-8'):
        """Funcion para delvolver digest response de un usuario."""
        nonce = self.get_nonce(username)
        digest = hashlib.sha224()
        digest.update(bytes(nonce[:len(nonce)//8], encoding))
        digest.update(bytes(self.passwords[username], encoding))
        digest.digest()
        return digest.hexdigest()

    def resent(self, data, dst):
        """Funcion para reenviar un mensaje a un cliente determinado."""
        ip = self.reg[dst]['ip']
        port = self.reg[dst]['port']
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, port))
            my_socket.send(bytes(data, 'utf-8'))
            sent_to(ip, port, data, fichero_log)
            response = my_socket.recv(1024).decode('utf-8')
            received_from(ip, port, response, fichero_log)
        return response

    def invite(self, data):
        """Funcion para tratar los mensajes de tipo invite."""
        user_dst = data.split()[1].split(':')[1]
        user_src = data.split('\r\n')[5].split('=')[1].split()[0]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if user_dst in self.reg and user_src in self.reg:
            response = self.resent(data, user_dst)
            sent_to(client_ip, client_port, response, fichero_log)
            self.wfile.write(bytes(response, 'utf-8'))
        else:
            line = 'SIP/2.0 404 User Not Found\r\n\r\n'
            sent_to(client_ip, client_port, line, fichero_log)
            self.wfile.write(bytes(line, 'utf-8'))

    def ack(self, data):
        """Funcion para tratar los mensajes de tipo ack."""
        user_dst = data.split()[1].split(':')[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if user_dst in self.reg:
            response = self.resent(data, user_dst)
        else:
            line = 'SIP/2.0 404 User Not Found\r\n\r\n'
            sent_to(client_ip, client_port, line, fichero_log)
            self.wfile.write(bytes(line, 'utf-8'))

    def bye(self, data):
        """Funcion para tratar los mensajes de tipo bye."""
        user_dst = data.split()[1].split(':')[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if user_dst in self.reg:
            response = self.resent(data, user_dst)
            sent_to(client_ip, client_port, response, fichero_log)
            self.wfile.write(bytes(response, 'utf-8'))
        else:
            line = 'SIP/2.0 404 User Not Found\r\n\r\n'
            sent_to(client_ip, client_port, line, fichero_log)
            self.wfile.write(bytes(line, 'utf-8'))

    def subscribe(self, data):
        """Funcion para tratar los mensajes subscribe."""
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        user = data.split()[1].split(':')[1]
        client_ip = self.reg[user]['ip']
        client_port = self.reg[user]['port']
        sent_to(client_ip, client_port, 'SIP/2.0 200 OK\r\n\r\n', fichero_log)
        if user not in self.subscriptions:
            self.subscriptions.append(user)

    def message(self, data):
        """Funcion para tratar los mensajes message."""
        user_dst = data.split()[1].split(':')[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if user_dst in self.reg:
            self.resent(data, user_dst)
        else:
            line = 'SIP/2.0 404 User Not Found\r\n\r\n'
            sent_to(client_ip, client_port, line, fichero_log)
            self.wfile.write(bytes(line, 'utf-8'))

    def handle(self):
        """Funcion para manejar todos los mensajes que llegan."""
        self.json2register()
        self.json2passwords()
        self.delete_client()
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        data = self.rfile.read().decode('utf-8')
        received_from(client_ip, client_port, data, fichero_log)
        print(data)
        metodo = data.split()[0]

        if metodo == 'REGISTER':
            self.register(data)
        elif metodo == 'INVITE':
            self.invite(data)
        elif metodo == 'ACK':
            self.ack(data)
        elif metodo == 'BYE':
            self.bye(data)
        elif metodo == 'SUBSCRIBE':
            self.subscribe(data)
        elif metodo == 'MESSAGE':
            self.message(data)
        else:
            r = 'register' in data.lower()
            i = 'invite' in data.lower()
            a = 'ack' in data.lower()
            b = 'bye' in data.lower()
            s = 'subscribe' in data.lower()
            m = 'message' in data.lower()
            if r or i or a or b or s or m:
                line = 'SIP/2.0 400 Bad Request\r\n\r\n'
                sent_to(client_ip, client_port, line, fichero_log)
                self.wfile.write(bytes(line, 'utf-8'))
            else:
                line = 'SIP/2.0 405 Method Not Allowed\r\n\r\n'
                sent_to(client_ip, client_port, line, fichero_log)
                self.wfile.write(bytes(line, 'utf-8'))

        self.register2json()


if __name__ == '__main__':

    try:

        if len(sys.argv) != 2:
            raise NameError
        config = sys.argv[1]

        if not os.path.exists(config):
            raise FileNotFoundError

        parser = make_parser()
        handler = XMLReader()
        parser.setContentHandler(handler)
        parser.parse(open(config))
        tags = handler.get_tags()
        ip = tags['server_ip']
        name = tags['server_name']
        port = int(tags['server_puerto'])
        fichero_log = tags['log_path']

    except ValueError:
        error('Port must be a number', fichero_log)
        finishing(fichero_log)
        sys.exit('Port must be a number')

    except FileNotFoundError:
        error('File ' + config + ' not exist', fichero_log)
        finishing(fichero_log)
        sys.exit('File ' + config + ' not exist')

    except NameError:
        sys.exit('Usage Error: python3 proxy_registrar.py config')
    starting(fichero_log)
    serv = socketserver.UDPServer((ip, port), SIPProxyHandler)

    print('Server ' + name + ' listening at port' + str(port) + '...')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        finishing(fichero_log)
        print('Finalizado servidor')
