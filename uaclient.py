#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys
import time
import os
import hashlib
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def write_log(line, fichero_log):
    """Funcion para escribir en los ficheros."""
    now = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
    with open(fichero_log, 'a') as log_file:
        log_file.write(now + ' ' + line.replace('\r\n', ' ') + '\n')


def sent_to(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes enviados."""
    line = 'Send to ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def received_from(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes recibidos."""
    line = 'Receive from ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def error(message, fichero_log):
    """Funcion para escribir los errores en los ficheros."""
    line = 'Error: ' + message
    write_log(line, fichero_log)


def starting(fichero_log):
    """Funcion para inicializar procesos."""
    line = 'Starting...'
    write_log(line, fichero_log)


def finishing(fichero_log):
    """Funcion para finalizar procesos."""
    line = 'Finishing.'
    write_log(line, fichero_log)


class XMLReader(ContentHandler):
    """Funcion para parsear ficheros 'xml'."""

    def __init__(self):
        """Funcion para inicializar xml reader."""
        self.current_tag = ''
        self.dicc = {}
        self.attr = {'account': ['username', 'passwd'],
                     'uaserver': ['ip', 'puerto'],
                     'rtpaudio': ['puerto'],
                     'regproxy': ['ip', 'puerto'],
                     'log': [''],
                     'audio': ['']}

    def startElement(self, name, attrs):
        """Funcion que se ejecuta al empezar una etiqueta."""
        self.current_tag = name
        if name in self.attr:

            for atributo in self.attr[name]:
                if atributo != '':
                    self.dicc[name + '_' + atributo] = attrs.get(atributo, '')

    def endElement(self, name):
        """Funcion que se ejecuta al acabar una etiqueta."""
        self.current_tag = ''

    def characters(self, content):
        """Funcion que se ejecuta para leer el contenido de una etiqueta."""
        if self.current_tag in ['log', 'audio']:
            self.dicc[self.current_tag] = content

    def get_tags(self):
        """Funcion para obtener etiquetas."""
        return self.dicc


def register(option, response=''):
    """Funcion que devuelve los mensajes register."""
    line = 'REGISTER sip:' + username + ':' + str(server_port)
    line += ' SIP/2.0\r\nExpires: ' + str(option) + '\r\n'
    if response != '':
        line += 'Authorization: Digest response="' + response + '"\r\n\r\n'
    else:
        line += '\r\n'
    return line


def sdp():
    """Funcion que devuelve el cuerpo SDP de un mensaje SIP."""
    line = 'v=0\r\no=' + username + ' ' + server_ip + '\r\n'
    line += 's=misesion\r\nt=0\r\nm=audio ' + str(rtp_port)
    line += ' RTP\r\n'
    header = 'Content-Type: application/sdp\r\nContent-Length: '
    return header + str(len(line)) + '\r\n\r\n' + line + '\r\n\r\n'


def invite(destino):
    """Funcion que devuelve los mensajes invite."""
    return 'INVITE sip:' + destino + ' SIP/2.0\r\n' + sdp()


def ack(destino):
    """Funcion que devuelve los mensajes ack."""
    return 'ACK sip:' + destino + ' SIP/2.0\r\n\r\n'


def bye(destino):
    """Funcion que devuelve los mensajes bye."""
    return 'BYE sip:' + destino + ' SIP/2.0\r\n\r\n'


def subscribe():
    """Funcion que devuelve los mensajes subscribe."""
    return 'SUBSCRIBE sip:' + username + ' SIP/2.0\r\n\r\n'


def message(option, text):
    """Funcion que devuelve los mensajes message."""
    msg = ''
    for word in text:
        msg += word + ' '
    mess = 'MESSAGE sip:' + option + ' SIP/2.0\r\nContent-Type: text/plain'
    mess += '\r\nContent-Length: ' + str(len(msg)) + '\r\n\r\n' + msg
    return mess + '\r\n\r\n'


def sip_mess(metodo, destino):
    """Funcion que devuelve un mensaje cualquiera."""
    return metodo.upper() + ' sip:' + destino + ' SIP/2.0\r\n\r\n'


def get_response(nonce, encoding='utf-8'):
    """Funcion que devuelve el digest response."""
    digest = hashlib.sha224()
    digest.update(bytes(nonce, encoding))
    digest.update(bytes(tags['account_passwd'], encoding))
    digest.digest()
    return digest.hexdigest()


if __name__ == '__main__':

    try:

        if len(sys.argv) != 4:
            if len(sys.argv) != 3:
                if sys.argv[2].lower() != 'message':
                    raise NameError
                else:
                    option = sys.argv[3]
                    text = sys.argv[4:]
        else:
            option = sys.argv[3]
        config = sys.argv[1]
        method = sys.argv[2]
        if not os.path.exists(config):
            raise FileNotFoundError

        parser = make_parser()
        handler = XMLReader()
        parser.setContentHandler(handler)
        parser.parse(open(config))
        tags = handler.get_tags()
        proxy_ip = tags['regproxy_ip']
        proxy_port = int(tags['regproxy_puerto'])
        filename = tags['log']
        starting(filename)
        username = tags['account_username']
        server_ip = tags['uaserver_ip']
        server_port = int(tags['uaserver_puerto'])
        rtp_port = int(tags['rtpaudio_puerto'])

    except ValueError:
        error('Port must be a number', filename)
        finishing(filename)
        sys.exit('Port must be a number')

    except FileNotFoundError:
        error('File ' + config + ' not exist', filename)
        finishing(filename)
        sys.exit('File ' + config + ' not exist')

    except NameError:
        sys.exit('Usage Error: python3 uaclient.py config method option')

    if method.lower() == 'register':
        msg = register(int(option))

    elif method.lower() == 'invite':
        msg = invite(option)

    elif method.lower() == 'bye':
        msg = bye(option)

    elif method.lower() == 'subscribe':
        msg = subscribe()

    elif method.lower() == 'message':
        msg = message(option, text)

    else:
        msg = sip_mess(method, option)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((proxy_ip, proxy_port))
        print('Enviando:', msg)
        my_socket.send(bytes(msg, 'utf-8'))
        sent_to(proxy_ip, proxy_port, msg, filename)
        if method.lower() == 'message':
            finishing(filename)
            sys.exit('Socket Terminado.')
        data = my_socket.recv(1024).decode('utf-8')
        received_from(proxy_ip, proxy_port, data, filename)
        print('Recibido -- ', data)

        if '100 Trying' in data and '180 Ringing' in data and '200 OK' in data:
            msg = ack(option)
            my_socket.send(bytes(msg, 'utf-8'))
            sent_to(proxy_ip, proxy_port, msg, filename)
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as msck:
                msck.connect((server_ip, server_port))
                msck.send(bytes(data, 'utf-8'))
                sent_to(server_ip, server_port, data, filename)
        elif '401 Unauthorized' in data:
            nonce = data.split('"')[1]
            response = get_response(nonce[:len(nonce)//8])
            msg = register(option, response)
            my_socket.send(bytes(msg, 'utf-8'))
            sent_to(proxy_ip, proxy_port, msg, filename)
            data = my_socket.recv(1024).decode('utf-8')
            received_from(proxy_ip, proxy_port, data, filename)
            print('Recibido -- ', data)

    print('Socket terminado.')
    finishing(filename)
