#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
import time
import os
import random
import simplertp
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


def write_log(line, fichero_log):
    """Funcion para escribir en los ficheros."""
    now = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
    with open(fichero_log, 'a') as log_file:
        log_file.write(now + ' ' + line.replace('\r\n', ' ') + '\n')


def sent_to(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes enviados."""
    line = 'Send to ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def received_from(ip, port, message, fichero_log):
    """Funcion para escribir en el fichero los mensajes recibidos."""
    line = 'Receive from ' + ip + ':' + str(port) + ': ' + message
    write_log(line, fichero_log)


def error(message, fichero_log):
    """Funcion para escribir los errores en los ficheros."""
    line = 'Error: ' + message
    write_log(line, fichero_log)


def starting(fichero_log):
    """Funcion para inicializar procesos."""
    line = 'Starting...'
    write_log(line, fichero_log)


def finishing(fichero_log):
    """Funcion para finalizar procesos."""
    line = 'Finishing.'
    write_log(line, fichero_log)


class XMLReader(ContentHandler):
    """Funcion para parsear ficheros 'xml'."""

    def __init__(self):
        """Funcion para inicializar xml reader."""
        self.current_tag = ''
        self.dicc = {}
        self.attr = {'account': ['username', 'passwd'],
                     'uaserver': ['ip', 'puerto'],
                     'rtpaudio': ['puerto'],
                     'regproxy': ['ip', 'puerto'],
                     'log': [''],
                     'audio': ['']}

    def startElement(self, name, attrs):
        """Funcion que se ejecuta al empezar una etiqueta."""
        self.current_tag = name
        if name in self.attr:

            for atributo in self.attr[name]:
                if atributo != '':
                    self.dicc[name + '_' + atributo] = attrs.get(atributo, '')

    def endElement(self, name):
        """Funcion que se ejecuta al acabar una etiqueta."""
        self.current_tag = ''

    def characters(self, content):
        """Funcion que se ejecuta para leer el contenido de una etiqueta."""
        if self.current_tag in ['log', 'audio']:
            self.dicc[self.current_tag] = content

    def get_tags(self):
        """Funcion para obtener etiquetas."""
        return self.dicc


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Servidor de envio rtp."""

    rtp = []

    def invite(self, data):
        """Funcion para tratar los mensajes invite."""
        port = int(data.split('\r\n')[8].split()[1])
        ip = data.split('\r\n')[5].split()[1]
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        self.rtp.append(ip)
        self.rtp.append(port)
        line = 'SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\n'
        line += 'SIP/2.0 200 OK\r\n' + self.sdp()
        sent_to(client_ip, client_port, line, fichero_log)
        self.wfile.write(bytes(line, 'utf-8'))

    def send_rtp(self, data=''):
        """Funcion para enviar paquetes rtp."""
        if data != '':
            self.rtp.append(data.split('\r\n')[9].split()[1])
            self.rtp.append(int(data.split('\r\n')[12].split()[1]))

        csrc = []
        for i in range(8):
            csrc.append(random.randint(2, 70))
        ALEAT = random.randint(0, 99)
        RTP_header = simplertp.RtpHeader()
        RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0,
                              marker=0, ssrc=ALEAT)
        RTP_header.setCSRC(csrc)
        audio = simplertp.RtpPayloadMp3(fichero_audio)
        simplertp.send_rtp_packet(RTP_header, audio, self.rtp[0], self.rtp[1])
        os.system('cvlc rtp://@' + self.rtp[0] + ':' + str(self.rtp[1]) + ' &')
        if data != '':
            self.rtp = []

    def bye(self, data):
        """Funcion para tratar los mensajes de tipo bye."""
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        if self.rtp != []:
            self.rtp = []
            sent_to(client_ip, client_port, 'SIP/2.0 200 OK', fichero_log)
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        else:
            line = 'SIP/2.0 404 User Not Found\r\n\r\n'
            sent_to(client_ip, client_port, line, fichero_log)
            self.wfile.write(bytes(line, 'utf-8'))

    def notify(self, data):
        """Funcion para tratar los mensajes de tipo notify."""
        text = data.split('"')[1]
        print('text:', text)
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        sent_to(client_ip, client_port, 'SIP/2.0 200 OK', fichero_log)
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')

    def message(self, data):
        """Funcion para tratar los mensajes de tipo message."""
        text = data.split('\r\n')[4]
        print('message:', text)

    def handle(self):
        """Manejador."""
        data = self.rfile.read().decode('utf-8')
        client_ip = self.client_address[0]
        client_port = self.client_address[1]
        received_from(client_ip, client_port, data, fichero_log)
        metodo = data.split()[0]
        print(metodo, 'received')
        trying = '100 Trying' in data
        ringing = '180 Ringing' in data
        ok = '200 OK' in data

        if metodo == 'INVITE':
            self.invite(data)
        elif metodo == 'ACK':
            self.send_rtp()
        elif metodo == 'BYE':
            self.bye(data)
        elif metodo == 'NOTIFY':
            self.notify(data)
        elif metodo == 'MESSAGE':
            self.message(data)
        elif trying and ringing and ok:
            self.send_rtp(data)

    def sdp(self):
        """Funcion para crear el cuerpo sdp."""
        line = 'v=0\r\no=' + username + ' ' + server_ip + '\r\n'
        line += 's=misesion\r\nt=0\r\nm=audio ' + str(rtp_port)
        line += ' RTP\r\n'
        header = 'Content-Type: application/sdp\r\nContent-Length: '
        return header + str(len(line)) + '\r\n\r\n' + line


if __name__ == '__main__':

    try:

        if len(sys.argv) != 2:
            raise NameError
        config = sys.argv[1]

        if not os.path.exists(config):
            raise FileNotFoundError

        parser = make_parser()
        handler = XMLReader()
        parser.setContentHandler(handler)
        parser.parse(open(config))
        tags = handler.get_tags()
        server_ip = tags['uaserver_ip']
        server_port = int(tags['uaserver_puerto'])
        username = tags['account_username']
        rtp_port = int(tags['rtpaudio_puerto'])
        fichero_audio = tags['audio']
        fichero_log = tags['log']

    except ValueError:
        error('Port must be a number', fichero_log)
        sys.exit('Port must be a number')

    except FileNotFoundError:
        sys.exit('File ' + config + ' not exist')

    except NameError:
        sys.exit('Usage Error: python3 uaserver.py config')
    starting(fichero_log)
    serv = socketserver.UDPServer((server_ip, server_port), SIPRegisterHandler)

    print('Listening...')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        finishing(fichero_log)
        print('Finalizado servidor')
